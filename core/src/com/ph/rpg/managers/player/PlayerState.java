package com.ph.rpg.managers.player;

/**
 * Created by Hamish on 2016-05-20.
 */
public enum PlayerState {
    Running,
    Idle
}
